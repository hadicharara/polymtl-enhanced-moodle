let usernameField = document.getElementById("code");
let passwordField = document.getElementById("nip");
let naissanceField = document.getElementById("naissance");

usernameField.addEventListener("input", login);
passwordField.addEventListener("input", login);
naissanceField.addEventListener("input", login);

function login(e) {
    if (
        usernameField.value != "" &&
        passwordField.value != "" &&
        naissanceField.value != ""
    ) {
        naissanceField.form.submit();
    }
}
